# Exercice d'Algorithmique avec correction

## Exercice
[**Liste des exercices**](https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/algo/exercices/index.html)

******************
## Corrections

### Enchaînements d’instructions

### Chaînes de caractères

```python
def compte_car(s, c):
    """
    :entrée s: str
    :entrée c: str
    :sortie n: int
    :pré-cond: len(c) == 1
    :post-cond: n est le nombre de valeurs i telles que s[i] == c
    """
```

```python
def compter_car(s, c):
    i = 0
    compteur = 0
    while i < len(s):
        if s[i] == c:
            compteur = compteur + 1
        i = i + 1
    return compteur
```

---

```python
def compte_minuscules(s):
    """
    :entrée s: str
    :sortie n: int
    :post-cond: n est le nombre valeurs de i telles que s[i] est une minuscule.
    """
    i = 0
    compteur = 0
    while i < len(s):
        if s[i] >='a' and s[i] <= 'z':
            compteur = compteur + 1
        i = i + 1
    return compteur
```
---

```python
def indice-min-car(s, c):
    """
    :entrée s: str
    :entrée c: str
    :sortie imin: int ou None
    :pré-cond: len(c) == 1
    :post-cond: imin est la plus petite valeur telle que s[imin] == c , ou None si s ne contient pas c.
    """

    chaine = 's'
    imin = '0'
    if s[imin] == c:
       imin = "c"
    return imin
```

---

```python
def commence_par(s, t):
    """
    :entrée s: str
    :entrée t: str
    :sortie c: bool
    :post-cond: c est True si et seulement si pour tout i tel que 0 = i < len(t) , s[i] == t[i] .
    """

    i = 0
    c = True
    while i < len(t) :
        if s[i] == t[i] :
            return c
        i = i + 1
    return False </code></pre>
```

---

```python
def inverse(s):
    """
    :entrée s: str
    :sortie t: str
    :post-cond: len(t) == len(s) et pour tout i tel que 0 = i < len(s), t[i] == s[-i-1] .
    """

    i = 0
    t= ""
    while i < len(s) :
        t = t + s[len(s) - i - 1]
        i = i + 1
    return t
```

---

```python
def indice-min-car(s, c):
    """
    :entrée s: str
    :entrée c: str
    :sortie imin: int ou None
    :pré-cond: len(c) == 1
    :post-cond: imin est la plus petite valeur telle que s[imin] == c , ou None si s ne contient pas c.
    """

    i = 0
    imin=""
    while i < len(s):
        if s[i] == c:
            imin = i
            return imin
        i = i + 1 </code></pre>
```

---

```python
def indice-max-car(s, c):
    """
    :entrée s: str
    :entrée c: str
    :sortie imin: int ou None
    :pré-cond: len(c) == 1
    :post-cond: imin est la plus petite valeur telle que s[imin] == c ,ou None si s ne contient pas c.
    """

    i = 0
    imax=""
    while i < len(s):
        if s[i] == c:
            imax = i
        i = i + 1
    if imax == "":
        imax = None
    return imax </code></pre>
```

---

```python
def palindrome(s):
    """
    :entrée s: str
    :sortie p: bool
    :post-cond: p est True si et seulement si pour tout i tel que 0 = i < len(s), s[i] == s[-i-1] .
    """

    i = 0
    p = True
    while i < len(s):
        if s[i] != s[-i - 1]:
            return None
        i = i + 1
    return p
```

---

```python
def compte_mots(phrase):

    prec, char = ' ', ' '
    nb_mots = 0
    idx = 0
    while idx < len(phrase):
        char = phrase[idx]
        prec = phrase[idx - 1]
        idx += 1
        if prec == ' ' and char != ' ':
            nb_mots += 1
    return nb_mots
```

---

### Appels et passages de paramètres

### Tableaux

### Récursivité

### Tries

### Types abstraits
