# MEMO SQL

***

`SELECT`  Permet de choisir les attributs à afficher
> <b>*</b> indiquant tous les attributs

`FROM` Suivi d'une ou plusieurs noms de table

* Ex: *Affiche le nom et le prénom de tous les élèves*

```sql
SELECT nomEleve, prenomEleve
FROM Eleves;
```
* *Affiche tous les articles (toutes les colonnes)*
```sql
SELECT *
FROM Article;
```

`WHERE` Permet de choisir certains attributs selon des critères *séparés par AND ou OR*
> Permet aussi de réaliser des jointures

> Les cotes sont obligatoires pour entourer les chaînes de caractères et les dates.

* Ex: *Affiche la désignation de tous les articles de la commande numéro 1258*
```sql
SELECT Designation
FROM Article, Commande
WHERE Article.numArt = Commande.numArt *Jointure*
AND numCom = 1258;
```

`DISTINCT` Permet d’éliminer les doublons

***

`ORDER BY` Trie les résultats par ordre croissant `ASC` ou décroissant `DESC`.
> Le tri peut se faire sur un ou plusieurs attributs.

* Ex: *Afficher la liste des employés par salaire décroissant, puis afficher les élèves par ordre alphabétique du nom puis du prénom*

```sql
SELECT nomEmpl, salaire FROM Employe
ORDER BY salaire DESC;
```

```sql
SELECT * FROM Eleves
ORDER BY nomEleve, prenomEleve;
```

`IN` indique si la valeur est égale à l'une de celles qui se trouvent entre parenthèses
> Peuvent être les résultats d'une sous-requête.
> On peut utiliser aussi NOT IN *la valeur n'est égale à aucune de celles de la liste*

* Ex: *Liste des employés occupant la fonction de programmeur, analyste ou développeur*

```sql
SELECT nomEmpl, numEmpl
FROM Employe
WHERE fonction
IN ('programmeur', 'analyste', 'developpeur');
```

***

`LIKE` Permet de comparer une valeur avec une chaîne non complète.
> % désigne plusieurs caractères
> _ désigne un seul caractère quelconque (ou aucun)

* Ex: *Recherche tous les noms commençant par DE*

```sql
SELECT nomCom, prenomCom, rueCom
FROM Commercants
WHERE nomCom LIKE 'DE%';
```

`UPPER` transforme tous les caractères d’un champ en majuscule.
> Attention, il faut marquer le nom voulu en majuscule.

* Ex:

```sql
SELECT numArt
FROM Article
WHERE UPPER (couleur) = 'BLEU';
```

`LOWER` les transforme en minuscule.
> Attention, il faut marquer le nom voulu enminuscule.

* Ex:

```sql
SELECT NumArt
FROM Article
WHERE LOWER (couleur) = 'blanc';
```
***

`BETWEEN` Indique si une valeur est comprise entre deux valeurs.
> Les extrêmes sont inclus dans l'intervalle.

* Ex: *Articles qui coûtent entre 10 et 20 euros inclus.*

```sql
SELECT numArt, nomArt
FROM Article
WHERE prix
BETWEEN 10 AND 20;
```

`AS` Permet de renommer un attribut
> Seulement pour l'affichage de la réponse à la requête

* Ex: *Affiche la référence et la marge de tous les produits*

```sql
SELECT refProd, (prixVente – prixAchat)
AS Marge
FROM Produit;
```

`EXISTS` Permet de tester l'existence de données dans une sous-requête
> Si La sous-requête renvoie au moins une ligne, même remplie de marqueurs NULL, le prédicat est vrai. Dans le cas contraire le prédicat à la valeur faux
>(NOT EXISTS) pour tester l'absence de données dans la sousrequête

* Ex:

```sql
SELECT nomAttr1
FROM table1
WHERE EXISTS (SELECT * FROM table2 WHERE condition);
```

```sql
SELECT nomAttr1
FROM table1
WHERE NOT EXISTS (SELECT * FROM table2 WHERE condition);
```

***

`COUNT(*) COUNT(attribut)` Permet de compter le nombre de lignes résultats.

* Ex: *Compte le nombre d'élève habitant Lyon et appelle le résultat NbLyonnais*

```sql
SELECT COUNT (*) AS NbLyonnais
FROM Eleves
WHERE villeEleve = 'Lyon';
```

`SUM(attribut)` Permet d'additionner les valeurs d'une colonne numérique pour les attributs selectionnés.

* Ex: *Calcul la somme de tous les opérations de débit du compte 1259 le 17/11/18.*

```sql
SELECT SUM(montantOperation)
FROM Operations
WHERE compte = '1259'
AND date = '17/11/18';
```

`AVG(attribut)` Permet d'afficher la moyenne des valeurs d'une colonne pour les lignes sélectionnées.

* Ex: *Afficher le salaire moyen des analystes*

```sql
SELECT AVG(salaire)
FROM Employe
WHERE fonction = 'analyste';
```

`MAX(attribut) MIN(attribut)` Permet d'obtenir la valeur maximale (ou minimale) d'un attribut pour un ensemble d'attribut sélectionnés.

* Ex: *Affiche le salaire de la secrétaire la mieux payée :*

```sql
SELECT MAX(salaire)
FROM Employe
WHERE function = 'secretaire';
```

***

`GROUP BY` Permet de diviser la table en groupes, chaque groupe étant l’ensemble des attributs ayant une valeur commune.

* Ex: *Affiche le nombre de professeurs par bureau*

```sql
SELECT bureau, COUNT(nomProf)
FROM Profs
GROUP BY bureau;
```

`HAVING` Sert à préciser quels groupes doivent être sélectionnés.
> Cette clause se place après GROUP BY et le prédicat ne peut porter que sur des opérateurs (>,<,=, !=) ou des expressions figurant dans le GROUP BY.

* Ex: *Affiche les bureaux qui accueillent plus de 2 professeurs*

```sql
SELECT bureau
FROM Profs
GROUP BY bureau
HAVING COUNT (*) > 2;
```

***

`ANY` La comparaison est vraie si elle est vraie pour au moins un élément de l’ensemble.
> Elle est fausse si la requete ne retourne rien
 * Ex:

```sql
… WHERE expr1 = / != / < / > / <= / >= expr2
ANY (SELECT…);
```

`ALL` La comparaison est vraie si elle est vraie pour tous les éléments de l’ensemble.
* Ex:

```sql
… WHERE expr1 = / != / < / > / <= / >= expr2
ALL (SELECT…);
```
